from django.conf.urls import patterns, include, url
from django.conf import settings
from attendance.api import EntryResourceAttendanceLog

entry_resource = EntryResourceAttendanceLog()

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^api/', include(entry_resource.urls)),
    url(r'^search/', include('attendance.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

# Uncomment the next line to serve media files in dev.
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
                            url(r'^__debug__/', include(debug_toolbar.urls)),
                            )
