import django_filters
from models import AttendanceLog


class MpDataFilter(django_filters.FilterSet):

    class Meta:
        model = AttendanceLog
        fields = {'name_of_member': ['icontains'],
                  'constituency': ['icontains'],
                  'state': ['icontains']}


