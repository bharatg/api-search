from django.shortcuts import render_to_response
from forms import MpDataFilter
from models import AttendanceLog


def member_list(request):
    f = MpDataFilter(request.GET, queryset=AttendanceLog.objects.all())
    return render_to_response('search.html', {'filter': f})
