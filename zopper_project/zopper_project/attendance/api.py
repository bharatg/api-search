from tastypie.resources import ModelResource
from models import AttendanceLog
from tastypie.constants import ALL
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from tastypie.cache import SimpleCache

cache = SimpleCache(cache_name='resources', timeout=10)

class EntryResourceAttendanceLog(ModelResource):

    class Meta:
        queryset = AttendanceLog.objects.all()
        resource_name = 'entry'
        authorization = Authorization()
        filtering = {"name_of_member": ALL, "constituency": ALL, "state": ALL}
        cache = SimpleCache(timeout=10)