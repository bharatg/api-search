from django.contrib import admin
from django.contrib.admin import DateFieldListFilter
from .models import AttendanceLog, Csv
# Register your models here.


admin.site.register(AttendanceLog)
admin.site.register(Csv)
