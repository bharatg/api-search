 # -- coding: utf-8 --
from django.db import models
from model_utils.models import TimeStampedModel
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from tastypie.utils.timezone import now
from django.contrib.auth.models import User
from annoying.functions import get_object_or_None
from django.dispatch import receiver
from django.db.models.signals import post_save
import csv

from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^attendance\.helpers\.ContentTypeRestrictedFileField"])

from .helpers import ContentTypeRestrictedFileField, upload_and_rename_csv

csv_storage = FileSystemStorage(location=settings.CSV_FILE_PATH)


class AttendanceLog(TimeStampedModel):
    seat_no = models.CharField(max_length=16, blank=True, null=True)
    name_of_member = models.SlugField(max_length=256, unique=True)
    lok_sabha = models.CharField(max_length=16, blank=True, null=True)
    session = models.CharField(max_length=16, blank=True, null=True)
    state = models.SlugField(max_length=256, null=True, blank=True)
    constituency = models.SlugField(max_length=256, null=True, blank=True)
    total_sittings = models.CharField(max_length=16, blank=True, null=True)
    no_of_days_member_signed = models.CharField(max_length=16, blank=True, null=True)

    def __unicode__(self):
        if self.name_of_member:
            return self.name_of_member
        else:
            return self.id


class Csv(TimeStampedModel):
    name = models.CharField(max_length=64)
    description = models.TextField(blank=True, null=True)
    file = ContentTypeRestrictedFileField(upload_to=upload_and_rename_csv, storage=csv_storage,
                                          content_types=['text/csv'], max_length=1000)
    ignore_first_row = models.BooleanField('', default=True)

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Csv, self).save()




@receiver(post_save, sender=Csv)
def signal_handler(sender, **kwargs):
    csv_name = kwargs.get('instance')

    file_path = str(csv_name.file)
    file_ptr = open(file_path)
    data_reader = csv.reader(file_ptr, delimiter=',')
    try:
        for row in data_reader:
            if not (data_reader.line_num == 1 and csv_name.ignore_first_row is True):
                attend_data = AttendanceLog(seat_no=row[1].strip(), name_of_member=row[2].strip(), lok_sabha=row[3].strip(), session=row[4].strip(), state=row[5].strip(), constituency=row[6].strip(), total_sittings=row[7].strip(), no_of_days_member_signed=row[8].strip())
                attend_data.save()
    except Exception, e:
        print Exception.message
        error_dict = 'invalid_csv'
        print error_dict
    print 'Data finally processed'


